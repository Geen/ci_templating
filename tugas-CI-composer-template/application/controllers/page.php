<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class page extends CI_Controller {

    public function index()
    {
        $page = $this->uri->segment(3);
        $this->load->view('room/'.$page);
    }

}

/* End of file page.php */
?>