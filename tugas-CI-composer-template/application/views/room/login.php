<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>LOG IN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url('assets/admin.css') ?>">
    <script src="main.js"></script>
</head>
<body class="body-login">
    <h2>Login Form</h2>
    <form class="form-login">
        <div class="img-logo-login">
            <img src="<?php echo base_url('assets/login.png') ?>" alt="" class="login-logo">
        </div>
        <div class="login">
            <label> <b>Username</b> </label>
            <input type="text" placeholder="Enter Username">

            <label><b>Password</b></label>
            <input type="password" placeholder="Enter Password">

            <a href="<?=site_url('page/index/admin')?>"><button type="button">Login</button></a>
            <label><input type="checkbox">Remember me</label>
        </div>
    </form>

        <div class="login">
           <a href="<?=site_url('room/index')?>"> <button type="button" class="cancelbtn">Cancel</button></a>
            <span class="psw">Forgot <a href="#">password?</a> </span>
        </div>
   
</body>
</html>