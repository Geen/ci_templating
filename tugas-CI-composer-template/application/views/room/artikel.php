<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">
</head>
<body>
    <header>
        <?php $this->load->view('layout/header');?>
    </header>
    <main>
        <section class="index-banner">
            <div class="text-banner">
                <h2>I AM FREELANCE WEB</h2>
                <h1>Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui et nihil quo porro.</h1>
            </div>
        </section>
        <div class="wrapper">
            <section class="index-link-content">
                <a href="">
                    <div class="link-content-2">
                        <table> 
                            <img src="<?php echo base_url('assets/artikel1.jpg') ?>" alt="">
                            <h2>KEIN DORONG KEMAJUAN TEKNOLOGI INFORMASI</h2>
                            <tr>
                                <td>   
                                    <small>
                                        <p>Dipublikasikan pada tanggal 07 Juni 2016</P>
                                    </small>
                                </td>
                            </tr>
                            <tr>
                                <td class="content-text">
                                    <p>
                                        Jakarta (ANTARA News) – Komite Ekonomi dan Industri Nasional (KEIN) akan mendorong kemajuan teknologi informasi untuk menumbuhkan perekonomian nasional.Menurut Ketua KEIN Soetrisno Bachir, ketersediaan infrastruktur yang lengkap akan meningkatkan kesejahteraan rakyat, maka salah satu infrastruktur yang penting saat ini adalah bidang teknologi informasi. <br>
                                        “Kita ingin menilai apakah kebijakan dan perkembangan di bidang teknologi informasi di Indonesia sudah bagus atau masih ada kendala. Nantinya yang lain akan menyusul juga sebagai pembahasan,” ujar Soetrisno di Jakarta, Kamis. Menurut dia, hampir semua negara memanfaatkan kecanggihan teknologi guna memantau situasi dan perkembangan yang terjadi di dalam maupun luar negeri. <br>
                                        “Masyarakat yang cerdas teknologi, selain menjadi tolak ukur tingkat kemajuan negara, diharapkan juga mendorong pendapatan perekonomian nasional,” kata dia. Menurut Soetrisno, melalui kemajuan teknologi dapat digunakan sebagai pembuka akses lapangan kerja. “Untuk itu segala aspek yang dibutuhkan agar teknologi informasi menguatkan infrastruktur perlu diketahui, termasuk apa yang masih mengganjal, apakah perizinannya, regulasi atau lainnya,” kata dia. <br>
                                        Sementara itu, pakar tekonologi informasi Onno W Purbo mengatakan perlu kebijakan yang berpihak pada pelaku dan perkembangan teknologi informasi saat ini. Onno yang juga pelaku teknologi informasi masih merasakan adanya aturan-aturan penghambat guna mengembangkan teknologi informasi. <br>
                                        “Dengan begitu masih sulit nantinya bila ingin mengubah paradigma masyarakat Indonesia dari yang tradisional ke basis teknologi,” ujar dia. Menurut Onno,sebenarnya mudah saja mencetak “melek” teknologi apalagi dengan konsep pembangunan pemerintah saat ini memulai dari pinggiran. <br>
                                        “Misalnya semua Sekolah Dasar (SD) dan desa sudah harus tersedia internet. Jadi mudah mengetahui apa masalah-masalahnya dan dilaporkan ke pemerintah pusat,” ujar Onno. Dia mengatakan memulai pengembangan teknologi informasi, seperti internet ke SD akan meningkatkan efisiensi. Buku-buku konvensional yang selama ini biayanya mahal akan digantikan dengan teknologi informasi yang terjangkau dan mudah di akses para murid Mengenai upaya meningkatkan perekonomian nasional, Onno mengungkapkan penekanannya kepada mahasiswa yang akan memasuki lingkungan kerja. <br>
                                        Bila kebijakan teknologi informasi menyasar mahasiswa, maka setelah mereka sarjana akan mengerti gunanya teknologi informasi dimanfaatkan sebagai lapangan pekerjaan. “Dengan begitu tingkat pertumbuhan ekonomi meningkat karena angka pengangguran ditekan,” ujar Onno.
                                    </p>
                                </td>
                            </tr>
                        </table>    
                    </div>
                </a>
            </section>
        </div>
    </main>

    <footer>
        <?php $this->load->view('layout/footer');?>
    </footer>
</body>
</html>