<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Our Team</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url('assets/main.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">
</head>
<body>
    <header>
        <?php $this->load->view('layout/header');?>
    </header>

    <div class="team-section">
        <h1>Our Team</h1>
        <span class="border"></span>
    <div class="png">
        <a href="#p1"><img src="<?php echo base_url('assets/ronaldo.png') ?>" alt=""></a>
        <a href="#p2"><img src="<?php echo base_url('assets/messi.png') ?>" alt=""></a>
        <a href="#p3"><img src="<?php echo base_url('assets/shahrukhan.png') ?>" alt=""></a>
    </div>
    <div class="section" id="p1">
        <span class="name">Cristian Ronaldo</span>
        <span class="border"></span>
        <p>
            Cristiano Ronaldo dos Santos Aveiro, lahir di Funchal, Madeira, Portugal, 5 Februari 1985; umur 33 tahun) atau lebih dikenal Cristiano Ronaldo merupakan seorang pemain sepak bola Portugal. Ia dapat berposisi sebagai sayap kiri atau kanan serta penyerang tengah. Saat ini ia bermain untuk tim Italia, Juventus dan untuk tim nasional Portugal. Sebelum bermain untuk Real Madrid, ia pernah bermain di Sporting Lisboa dan Manchester United. Pemain yang kerap bernomor punggung 7 di lapangan hijau ini juga akrab dengan sebutan CR7, gabungan dari inisial nama dan nomor punggungnya.

            Dia memakai nomor punggung 7 di United, yang sebelumnya dikenakan oleh Johnny Berry, George Best, Steve Coppell, Bryan Robson, Eric Cantona dan David Beckham. Setelah menghabiskan tahun pertamanya di Madrid mengenakan nomor punggung 9, ia mulai mengenakan nomor 7 lagi menyusul kepergian pemain legendaris Raul Gonzalez.

            Segudang prestasi telah berhasil dia raih. Cristiano Ronaldo berhasil meraih gelar FIFA Ballon D’Or di bulan Januari 2015 lalu yang diadakan di markas FIFA Zurich, Swiss. Gelar FIFA Ballon D’Or ini merupakan penghargaan ketiga bagi Ronaldo, setelah sebelumnya pada tahun 2013 lalu dan 2008 ketika masih berseragam Manchester United.
        </p>
    </div>
    <div class="section" id="p2">
        <span class="name">Lionel Messi</span>
        <span class="border"></span>
        <p>
            Lionel Andrés "Leo" Messi (pengucapan bahasa Spanyol: [ljoˈnel anˈdɾes ˈmesi], lahir di Rosario, 24 Juni 1987; umur 31 tahun) adalah seorang pemain sepak bola Argentina yang saat ini bermain untuk FC Barcelona dan merupakan kapten tim nasional sepak bola Argentina, bermain sebagai penyerang. Ia adalah pencetak gol terbanyak Barcelona sepanjang sejarah. Di usia 21 tahun, Messi telah dinominasikan untuk Ballon d'Or dan Pemain Terbaik Dunia FIFA. Pada 2009, ia memenangi Ballon d'Or dan Pemain Terbaik Dunia FIFA dan penghargaan pertama FIFA Ballon d`Or pada 2010 dan 2011..

            Messi adalah pemain keempat yang menjuarai tiga Ballon d`Or, dan pemain kedua yang memenangi Ballon d`Or dua kali berturut-turut. Ia telah menjuarai lima La Liga, dua Copa del Rey, lima Supercopa de Espana, tiga Liga Champions UEFA, dua Piala Super Eropa dan dua Piala Dunia Klub. Pada 2012, ia mencetak rekor Liga Champions UEFA dengan menjadi pemain pertama yang mencetak lima gol dalam satu pertandingan. Ia juga menyamai rekor 14 gol Jose Altafini di satu musim Liga Champions. Ia juga mencetak rekor untuk gol terbanyak dalam satu musim pada musim 2011-12, dengan 73 gol. Di musim yang sama, ia mencetak rekor pencetak gol terbanyak La Liga dalam satu musim, 50 gol.
        </p>
    </div>
    <div class="section" id="p3">
        <span class="name">Shahrul Khan</span>
        <span class="border"></span>
        <p>
            Shahrukh Khan (bahasa Hindi: शाहरुख़ ख़ान, bahasa Urdu: شاہ رخ خان, lahir di New Delhi, 2 November 1965; umur 52 tahun) atau biasa dikenal sebagai SRK, adalah seorang pemeran film, produser dan pembawa acara televisi asal India. Disebut dalam media sebagai "Baadshah of Bollywood", "King of Bollywood" atau "King Khan", ia tampil dalam lebih dari 80 film Bollywood, dan meraih sejumlah penghargaan, termasuk 14 Penghargaan Filmfare. Khan memiliki penggemar berjumlah signifikan di Asia dan diaspora India di seluruh dunia. Dalam hal jumlah audien dan pemasukan, ia disebut sebagai salah satu bintang film paling sukses di dunia.

            Khan memulai karirnya dengan tampil dalam beberapa serial televisi pada akhir 1980an. Ia membuat debut Bollywoodnya pada 1992 dengan film Deewana. Pada awal karirnya, Khan dikenal karena memerankan peran-peran jahat dalam film-film Darr (1993), Baazigar (1993) dan Anjaam (1994). Ia kemudian naik daun setelah membintangi serangkaian film percintaan, yang meliputi Dilwale Dulhania Le Jayenge (1995), Dil To Pagal Hai (1997), Kuch Kuch Hota Hai (1998), Mohabbatein (2000) dan Kabhi Khushi Kabhie Gham... (2001). Ia meraih sambutan meraih atas perannya sebagai seorang pemabuk dalam film Devdas (2002), seorang ilmuwan NASA dalam film Swades (2004), seorang pelatih hoki dalam film Chak De! India (2007) dan seorang pria pengidap sindrom Asperger dalam film My Name Is Khan (2010). Film-film berkeuntungan tertingginya meliputi film-film komedi Chennai Express (2013) dan Happy New Year (2014). Beberapa filmnya berisi tema-tema identitas nasional India dan hubungan dengan komunitas diaspora, atau perbedaan dan keragaman gender, ras, sosial dan agama. Untuk kontribusinya pada perfilman, Pemerintah India menganugerahinya dengan Padma Shri, dan Pemerintah Perancis menganugerahinya dengan Ordre des Arts et des Lettres dan Légion d'honneur
        </p>
    </div>
    </div>
</body>
</html>