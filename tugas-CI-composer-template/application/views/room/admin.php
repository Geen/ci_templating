<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>Admin</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/admin.css') ?>">
 </head>
 <body>
<header>
    <?php $this->load->view('layout/navbar');?>
</header>

<div class="content">
    <div class="main-content">
        <div class="title">
            
        </div>
        <div class="main">
            <div class="widget-1">
                <div class="title">Number of views</div>
                <div class="chart">
                <img src="<?php echo base_url('assets/icon1.png') ?>" alt=""><h4>777</h4></div>
            </div>
            <div class="widget-2">
                <div class="title">Number of likes</div>
                <div class="chart">
                    <img src="<?php echo base_url('assets/icon2.png') ?>" alt=""><h4>777</h4>
                </div>
            </div>
            <div class="widget-3">
                <div class="title">Number of comments</div>
                <div class="chart">
                    <img src="<?php echo base_url('assets/icon3.png') ?>" alt=""><h4>777</h4>
                </div>
            </div>
        </div>
        <div class="title"> <a href="<?=site_url('page/index/addartikel')?>"> <button type="submit">Add News</button></a></div>
            <table class="list">
                <tr align="center">
                    <th>JUDUL</th>
                    <th>ISI</th>
                    <th>ACTION</th>
                </tr>
                <tr>
                    <td>KEIN DORONG KEMAJUAN ....</td>
                    <td>Ketua Komite Ekonomi dan Industri Nasional (KEIN) Soetrisno Bachir (tengah) didampingi pengurus</td>
                    <td>
                        <button>EDIT</button>
                        <button>DELETE</button>
                    </td>
                </tr>
                <tr>
                    <td>6 LANGKAH MUDAH UNTUK ....</td>
                    <td>Suara.com – Internet ibarat pedang bermata dua. Jika tak hati-hati menggunakannya,</td>
                    <td>
                        <button>EDIT</button>
                        <button>DELETE</button>
                    </td>
                </tr>
                <tr>
                    <td>6 LANGKAH MUDAH UNTUK ....</td>
                    <td>HTML adalah inti dari seluruh halaman web. Sangat mustahil untuk membuat website</td>
                    <td>
                        <button>EDIT</button>
                        <button>DELETE</button>
                    </td>
                </tr>
            </table>  
        </div>
    </div>
  
</body>
</html>