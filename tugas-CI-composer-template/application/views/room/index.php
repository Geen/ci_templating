<?php $this->load->view('layout/header');?>
<link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">
<main>
    <section class="index-banner">
        <div class="text-banner">
            <h2>I AM FREELANCE WEB</h2>
            <h1>Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui et nihil quo porro.</h1>
        </div>
    </section>
    <div class="wrapper">
        <section class="index-link">
            <a href="<?=site_url('page/index/artikel')?>">
                <div class="link-content-2">
                    <table>
                        <img src="<?php echo base_url('assets/artikel1.jpg') ?>" alt="">
                    <h2>KEIN DORONG KEMAJUAN TEKNOLOGI INFORMASI</h2>
                        <tr>
                            <td>   
                                <small>
                                    <p>Dipublikasikan pada tanggal 07 Juni 2016</P>
                                </small>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    Ketua Komite Ekonomi dan Industri Nasional (KEIN) Soetrisno Bachir (tengah) didampingi pengurus memaparkan hasil pertemuan dengan Presiden Joko Widodo di Kantor Presiden, Jakarta, Selasa (7/6/2016). KEIN dalam pertemuan tersebut menyerahkan memo terkait perkembangan kondisi perekonomian terkini dan perbandingan dengan negara lain. 
                                    <a href="<?=site_url('page/index/artikel')?>">Selengkapnya....</a>
                                </p>
                            </td>
                        </tr>
                    </table>    
                </div>
            </a>
            <a href="<?=site_url('page/index/artikel')?>">
                <div class="link-content-2">
                    <table>
                        <img src="<?php echo base_url('assets/artikel2.jpg') ?>" alt="">
                    <h2>KEIN DORONG KEMAJUAN TEKNOLOGI INFORMASI</h2>
                        <tr>
                            <td>   
                                <small>
                                    <p>Dipublikasikan pada tanggal 07 Juni 2016</P>
                                </small>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    Ketua Komite Ekonomi dan Industri Nasional (KEIN) Soetrisno Bachir (tengah) didampingi pengurus memaparkan hasil pertemuan dengan Presiden Joko Widodo di Kantor Presiden, Jakarta, Selasa (7/6/2016). KEIN dalam pertemuan tersebut menyerahkan memo terkait perkembangan kondisi perekonomian terkini dan perbandingan dengan negara lain. 
                                    <a href="<?=site_url('page/index/artikel')?>">Selengkapnya....</a>
                                </p>
                            </td>
                        </tr>
                    </table>    
                </div>
            </a>
            <a href="<?=site_url('page/index/artikel')?>">
                <div class="link-content-2">
                    <table>
                        <img src="<?php echo base_url('assets/artikel1.jpg') ?>" alt="">
                    <h2>KEIN DORONG KEMAJUAN TEKNOLOGI INFORMASI</h2>
                        <tr>
                            <td>   
                                <small>
                                    <p>Dipublikasikan pada tanggal 07 Juni 2016</P>
                                </small>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    Ketua Komite Ekonomi dan Industri Nasional (KEIN) Soetrisno Bachir (tengah) didampingi pengurus memaparkan hasil pertemuan dengan Presiden Joko Widodo di Kantor Presiden, Jakarta, Selasa (7/6/2016). KEIN dalam pertemuan tersebut menyerahkan memo terkait perkembangan kondisi perekonomian terkini dan perbandingan dengan negara lain. 
                                    <a href="<?=site_url('page/index/artikel')?>">Selengkapnya....</a>
                                </p>
                            </td>
                        </tr>
                    </table>    
                </div>
            </a>
        </section>
    </div>
</main>
<?php $this->load->view('layout/footer');?>