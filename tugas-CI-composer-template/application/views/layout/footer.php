<footer>
        <ul class="footer-link">
            <li><a href="<?=site_url('room/index')?>">Home</a></li>
            <li><a href="<?=site_url('page/index/artikel')?>">Artikel</a></li>
            <li><a href="<?=site_url('page/index/artikel')?>">artikel2</a></li>
            <li><a href="<?=site_url('page/index/myteam')?>">About me</a></li>
            <li><a href="<?=site_url('page/index/login')?>">login</a></li>
        </ul>
        <ul class="footer-addres">
            <li>
                <p>
                    MALANG ( KANTOR PUSAT ) <br>
                    <br>
                    Jl. Soekarno Hatta Ruko Permata Griya Shanta NR. 24 – 25 <br>
                    Malang – Jawa Timur <br>
                    <br>
                    Telp: 0341 – 496 497, 488 890 <br>
                    <br>
                    Fax: 0341 – 408 657 <br>
                    <br>
                    Email: layanan@cendana2000.com <br>
                </p>
            </li>
        </ul>
        <ul class="footer-addres-2">
            <li>
                <p>
                    JAKARTA ( KANTOR CABANG ) <br>
                    <br>
                    Jl. Kebagusan Raya no 192, Pasar Minggu, Jakarta Selatan 12550 <br>
                    <br>
                    Telp: 021-22978922 <br>
                </p>
            </li>
        </ul>
        <div class="footer-sm">
            <a href="youtube.com">
                <img src="<?php echo base_url('assets/youtube.svg') ?>" alt="logo-youtube">
            </a>
            <a href="facebook.com">
                <img src="<?php echo base_url('assets/facebook.svg') ?>" alt="logo-facebook">
            </a>
            <a href="twitter.com">
                <img src="<?php echo base_url('assets/twitter.svg') ?>" alt="logo-twitter">
            </a>
        </div>
    </footer>